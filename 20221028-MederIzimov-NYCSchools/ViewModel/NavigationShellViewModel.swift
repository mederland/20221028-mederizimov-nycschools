//
//  NavigationShellViewModel.swift
//  20221028-MederIzimov-NYCSchools
//
//  Created by Consultant on 10/29/22.
//

import SwiftUI

@available(iOS 16.0, *)
class NavigationShellViewModel: ObservableObject {
    
    @Published var navigationPath = NavigationPath()
    //for the app, using "NavigationPath"
    
    lazy var schoolViewModel = {
        SchoolViewModel(router: self)
        //Serve as "router" and host the navigation
    }()
}
